<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        
        function generaColores($num,$alm=false){
            $col=array();
            for($n=0;$n<$num;$n++){
                $c=0;
                $lim=6;
                $col[$n]="";
                if($alm){
                    $col[$n]="#";
                    $lim=7;
                }
                for(;$c<$lim;$c++){
                    $col[$n].=dechex(mt_rand(0,15));
                }
            }
            return $col;
        }
        
        var_dump(generaColores(10));
        
        ?>
    </body>
</html>

