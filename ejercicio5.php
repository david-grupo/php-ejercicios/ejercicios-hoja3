<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        function elementosRepetidos($array,$dev=false){
            $rep=array();
            
            foreach ((array) $array as $value){
                $inArray=false;
                
                foreach ($rep as $i => $rItem){
                    if($rItem['value']===$value){
                        $inArray=true;
                        ++$rep[$i]['count'];
                    }
                }
                if(false===$inArray){
                    $i=count($rep);
                    $rep[$i]=array();
                    $rep[$i]['value']=$value;
                    $rep[$i]['count']=1;
                }
            }
            if(!$dev){
                foreach($rep as $i => $rItem){
                    if($rItem['count']===1){
                        unset($rep[$i]);
                    }
                }
            }
            sort($rep);
            
            return $rep;
        }
        
       $entrada=array(1,2,3,"a","a","b",1,1,1,1,1);
        var_dump(elementosRepetidos($entrada,TRUE));
 
        
        
        
        ?>
    </body>
</html>


